#!/usr/bin/env bash

OPENCOVER="./packages/OpenCover.4.6.519/tools/OpenCover.Console.exe"
FILTER="+[Fact] +[Theory]"
TARGET="mono"
ARGS="./packages/xunit.runner.console.2.1.0/tools/xunit.console.exe StoreApp.Tests/bin/Debug/StoreApp.Tests.dll -nologo -noshadow -parallel assemblies -appveyor"

OUTPUT="coverage.xml"

mono "${OPENCOVER}" -register:user -filter:"${FILTER}" -target:"${TARGET}" -targetargs:"${ARGS}" -skipautoprops -output:"${OUTPUT}"
