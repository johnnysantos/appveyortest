#addin Cake.Coveralls

#tool "nuget:?package=coveralls.io"
// #tool "nuget:?package=xunit.runner.console"
// #tool "nuget:?package=OpenCover"

using System.Collections.Generic
using System.Diagnostics
using System;

var target = Argument("target", "Default");
var coverallsToken = Argument("coverallsToken", "");
var buildConfig = Argument("buildconfiguration", "Debug");
var buildPath = "./build";
var times = new Dictionary<string, Stopwatch>();

// ### INICIO SETUP ###
TaskSetup((context, task) =>
{
	var tName = task.Task.Name;
	times[tName] = new Stopwatch();
	times[tName].Start();
});

TaskTeardown((context, task) =>
{
	var tName = task.Task.Name;
	times[tName].Stop();
	Information("========================================");
	Information("Duração Task: {0}", times[tName].Elapsed);
	times.Remove(tName);
});
// ### FIM SETUP ###

Task("Default")
  .Does(() =>
{
	Information("Hello World!");
});

Task("Build")
.IsDependentOn("Clean")
.Does(() => {
	OpenCover(tool => {
		tool.XUnit2("./**/App.Tests.dll",
			new XUnit2Settings {
				ShadowCopy = false
			});
	},
	new FilePath("./result.xml"),
	new OpenCoverSettings()
		.WithFilter("-[*.Tests]*")
	);
})
.OnError(ex => {
	Information("Aconteceu um erro no build em \"{2}\".{0}{1}{0}", Environment.NewLine, ex, buildPath);
	RunTarget("Clean");
});

Task("Clean").Does(() => {
	Information("Limpando o diretório de build: {0}", buildPath);
	CleanDirectory(buildPath);
});

Task("Run-Tests")
    .Does(() =>
{
	var testAssemblies = GetFiles("./src/**/bin/Release/*.Tests.dll");
	Information("Rodando os seguintes testes:{0}{1}", Environment.NewLine, testAssemblies);
	XUnit2(testAssemblies);
});

Task("Upload-Coverage-Report")
    .Does(() =>
{
	if(string.IsNullOrEmpty(coverallsToken))
		throw new ArgumentNullException("coverallsToken");
	#break
    CoverallsIo(new FilePath("coverage.xml"), new CoverallsIoSettings()
    {
        RepoToken = coverallsToken
    });
})
.OnError(ex => {
	Information("Não foi possível enviar os dados de coverage.");
	Information("Erro: {0}", ex);
});

if(!string.IsNullOrEmpty(target))
	RunTarget(target);