﻿using System;
using System.Web.Http.Results;
using System.Net;
using JetBrains.Annotations;
using StoreApp.Models;
using StoreApp.Controllers;
using Xunit;

namespace StoreApp.Tests
{
    public class TestProductController
    {
        [Fact]
        public void PostProduct_ShouldReturnSameProduct()
        {
            var controller = new ProductController(new TestStoreAppContext());

            var item = GetDemoProduct();

            var result =
                controller.PostProduct(item) as CreatedAtRouteNegotiatedContentResult<Product>;

            Assert.NotNull(result);
            Assert.Equal(result.RouteName, "DefaultApi");
            Assert.Equal(result.RouteValues["id"], result.Content.Id);
            Assert.Equal(result.Content.Name, item.Name);
        }

        [Fact]
        public void PutProduct_ShouldReturnStatusCode()
        {
            var controller = new ProductController(new TestStoreAppContext());

            var item = GetDemoProduct();

            var result = controller.PutProduct(item.Id, item) as StatusCodeResult;
            Assert.NotNull(result);
            Assert.IsAssignableFrom(typeof(StatusCodeResult), result);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }

        [Fact]
        public void PutProduct_ShouldFail_WhenDifferentID()
        {
            var controller = new ProductController(new TestStoreAppContext());

            var badresult = controller.PutProduct(999, GetDemoProduct());
            Assert.IsAssignableFrom(typeof(BadRequestResult), badresult);
        }

        [Fact]
        public void GetProduct_ShouldReturnProductWithSameID()
        {
            var context = new TestStoreAppContext();
            context.Products.Add(GetDemoProduct());

            var controller = new ProductController(context);
            var result = controller.GetProduct(3) as OkNegotiatedContentResult<Product>;

            Assert.NotNull(result);
            Assert.Equal(3, result.Content.Id);
        }

        [Fact]
        public void GetProducts_ShouldReturnAllProducts()
        {
            var context = new TestStoreAppContext();
            context.Products.Add(new Product {Id = 1, Name = "Demo1", Price = 20});
            context.Products.Add(new Product {Id = 2, Name = "Demo2", Price = 30});
            context.Products.Add(new Product {Id = 3, Name = "Demo3", Price = 40});

            var controller = new ProductController(context);
            var result = controller.GetProducts() as TestProductDbSet;

            Assert.NotNull(result);
            Assert.Equal(3, result.Local.Count);
        }

        [Fact]
        public void DeleteProduct_ShouldReturnOK()
        {
            var context = new TestStoreAppContext();
            var item = GetDemoProduct();
            context.Products.Add(item);

            var controller = new ProductController(context);
            var result = controller.DeleteProduct(3) as OkNegotiatedContentResult<Product>;

            Assert.NotNull(result);
            Assert.Equal(item.Id, result.Content.Id);
        }

        [NotNull]
        private static Product GetDemoProduct()
        {
            return new Product() {Id = 3, Name = "Demo name", Price = 5};
        }
    }
}