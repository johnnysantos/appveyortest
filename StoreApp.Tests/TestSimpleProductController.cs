﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Results;
using StoreApp.Controllers;
using StoreApp.Models;
using Xunit;

namespace StoreApp.Tests
{
    public class TestSimpleProductController
    {
        [Fact]
        public void GetAllProducts_ShouldReturnAllProducts()
        {
            var testProducts = GetTestProducts();
            var controller = new SimpleProductController(testProducts);

            var result = controller.GetAllProducts() as List<Product>;
            Assert.Equal(testProducts.Count, result.Count);
        }

        [Fact]
        public async Task GetAllProductsAsync_ShouldReturnAllProducts()
        {
            var testProducts = GetTestProducts();
            var controller = new SimpleProductController(testProducts);

            var result = await controller.GetAllProductsAsync() as List<Product>;
            Assert.Equal(testProducts.Count, result.Count);
        }

        [Fact]
        public void GetProduct_ShouldReturnCorrectProduct()
        {
            var testProducts = GetTestProducts();
            var controller = new SimpleProductController(testProducts);

            var result = controller.GetProduct(4) as OkNegotiatedContentResult<Product>;
            Assert.NotNull(result);
            Assert.Equal(testProducts[3].Name, result.Content.Name);
        }

        [Fact]
        public async Task GetProductAsync_ShouldReturnCorrectProduct()
        {
            var testProducts = GetTestProducts();
            var controller = new SimpleProductController(testProducts);

            var result = await controller.GetProductAsync(4) as OkNegotiatedContentResult<Product>;
            Assert.NotNull(result);
            Assert.Equal(testProducts[3].Name, result.Content.Name);
        }

        [Fact]
        public void GetProduct_ShouldNotFindProduct()
        {
            var controller = new SimpleProductController(GetTestProducts());

            var result = controller.GetProduct(999);
            Assert.IsAssignableFrom(typeof(NotFoundResult), result);
        }

        private List<Product> GetTestProducts()
        {
            var testProducts = new List<Product>();
            testProducts.Add(new Product {Id = 1, Name = "Demo1", Price = 1});
            testProducts.Add(new Product {Id = 2, Name = "Demo2", Price = 3.75M});
            testProducts.Add(new Product {Id = 3, Name = "Demo3", Price = 16.99M});
            testProducts.Add(new Product {Id = 4, Name = "Demo4", Price = 11.00M});

            return testProducts;
        }
    }
}